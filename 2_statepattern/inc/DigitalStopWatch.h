#ifndef DIGITALSTOPWATCH_H
#define DIGITALSTOPWATCH_H

typedef struct DigitalStopWatchADT* DigitalStopWatch;

DigitalStopWatch
DigitalStopWatchNew(void);

void
DigitalStopWatchDestroy(DigitalStopWatch* watchPtr);

void
DigitalStopWatchStart(DigitalStopWatch instance);

void
DigitalStopWatchStop(DigitalStopWatch instance);

#endif
