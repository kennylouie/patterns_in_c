#ifndef BASICWATCHSTATEPROTECTED_H
#define BASICWATCHSTATEPROTECTED_H

#include "WatchState/BasicWatchState/BasicWatchState.h"
#include "WatchState/WatchState.h"

struct BasicWatchStateCDT
{
	// access to parent class
	WatchState state;

	void (*start)(BasicWatchState state);
	void (*stop)(BasicWatchState state);
	void (*destroy)(BasicWatchState* statePtr);
};

#endif
