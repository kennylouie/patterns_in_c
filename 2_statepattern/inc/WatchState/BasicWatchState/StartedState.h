#ifndef BASICSTARTEDSTATE_H
#define BASICSTARTEDSTATE_H

#include "WatchState/BasicWatchState/BasicWatchState.h"

void
TransitionToStarted(BasicWatchState state);

#endif
