#ifndef BASICSTOPPEDSTATE_H
#define BASICSTOPPEDSTATE_H

#include "WatchState/BasicWatchState/BasicWatchState.h"

void
TransitionToStopped(BasicWatchState state);

#endif
