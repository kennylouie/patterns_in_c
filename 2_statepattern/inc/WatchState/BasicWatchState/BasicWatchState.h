#ifndef BASICWATCHSTATE_H
#define BASICWATCHSTATE_H

typedef struct BasicWatchStateCDT* BasicWatchState;

#include "WatchState/WatchState.h"

BasicWatchState
BasicWatchStateNew(WatchState state);

void
DefaultBasicWatchState(BasicWatchState state);

#endif
