#ifndef WATCHSTATE_H
#define WATCHSTATE_H

typedef struct WatchStateInterface* WatchState;

WatchState
WatchStateNew(void);

void
WatchStateDestroy(WatchState* statePtr);

char*
WatchStateGetClassName(WatchState instance);

void
WatchStateStart(WatchState instance);

void
WatchStateStop(WatchState instance);

#endif
