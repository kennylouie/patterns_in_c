#ifndef WATCHSTATEPROTECTED_H
#define WATCHSTATEPROTECTED_H

// This is the api used by derived classes

struct WatchStateInterface
{
	// access to derived concrete class
	void* instance;
	// class data and methods
	struct WatchStateClassMembers* vft;
};

struct WatchStateClassMembers
{
	char* className;

  void (*start)(void* instance);
  void (*stop)(void* instance);
	void (*destroy)(void* instance);
};

#endif
