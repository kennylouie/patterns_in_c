# State Pattern

## Background
The goal of state pattern is to have well defined states that the program can transition into, and although having the same api interface, can have different behaviours depending on the current state.

This pattern is also using the c inheritance coding pattern. There are interfaces between all major components of the watch. The component that manages the state is the `watchstate`, which can have different implementations. A basic one is provided but it is conceiveable that others can be written and easily swapped in due to the interface design. Likewise, a watch display interface can be written to extend this code.

## Usage

Example trivial program
```
make && ./bin/statepattern
```
