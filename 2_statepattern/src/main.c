#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "DigitalStopWatch.h"

int
main(void)
{
	DigitalStopWatch digitalStopWatch = DigitalStopWatchNew();

	DigitalStopWatchStart(digitalStopWatch);
	// should do nothing
	DigitalStopWatchStart(digitalStopWatch);

	DigitalStopWatchStop(digitalStopWatch);
	// should do nothing
	DigitalStopWatchStop(digitalStopWatch);

	// similar outcome as above
	DigitalStopWatchStart(digitalStopWatch);
	DigitalStopWatchStop(digitalStopWatch);

	DigitalStopWatchDestroy(&digitalStopWatch);

	return EXIT_SUCCESS;
}
