#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "WatchState/WatchState.h"
#include "WatchState/WatchStateProtected.h"

// defaults when concrete class isn't implemented
static void
virtualStart(void* instance) { fprintf(stderr, "error: call to virtual function Start\n"); }

static void
virtualStop(void* instance) { fprintf(stderr, "error: call to virtual function Stop\n"); }

static void
virtualDestroy(void* instance) { fprintf(stderr, "error: call to virtual function Destroy\n"); }

static struct WatchStateClassMembers
WatchStateVFT = {
	.className = "WatchState",
	.start = virtualStart,
	.stop = virtualStop,
	.destroy = virtualDestroy
};

static void
watchStateReset(WatchState instance)
{
	instance->instance = NULL;
	instance->vft = &WatchStateVFT;
}

WatchState
WatchStateNew(void)
{
	WatchState instance = malloc(sizeof *instance);
	watchStateReset(instance);
	return instance;
};

void
WatchStateDestroy(WatchState* statePtr)
{
	WatchState state = *statePtr;
	if (!state) { fprintf(stderr, "error: double delete of the watchstate interface\n"); exit(1); }

	void* inst = state->instance;
	if (inst)
	{
		state->vft->destroy(inst);
		watchStateReset(state);
	}

	free(state);
	*statePtr = NULL;
};

char*
WatchStateGetClassName(WatchState instance)
{
	return instance->vft->className;
}

void
WatchStateStart(WatchState instance)
{
	return instance->vft->start(instance->instance);
}

void
WatchStateStop(WatchState instance)
{
	return instance->vft->stop(instance->instance);
}
