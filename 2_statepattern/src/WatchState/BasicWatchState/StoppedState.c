#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "WatchState/BasicWatchState/BasicWatchState.h"
#include "WatchState/BasicWatchState/BasicWatchStateProtected.h"
#include "WatchState/BasicWatchState/StartedState.h"
#include "WatchState/BasicWatchState/StoppedState.h"

// The variants of the behaviour while in this state
static void
startWatch(BasicWatchState state)
{
	printf("Watch Started.\n");
	TransitionToStarted(state);
};

static void
stopWatch(BasicWatchState state)
{
	printf("Watch already stopped.\n");
};

void
TransitionToStopped(BasicWatchState state)
{
	assert(NULL != state);

	DefaultBasicWatchState(state);

	state->start = startWatch;
	state->stop = stopWatch;
};
