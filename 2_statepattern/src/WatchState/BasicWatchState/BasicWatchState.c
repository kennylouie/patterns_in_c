#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "WatchState/BasicWatchState/BasicWatchState.h"
#include "WatchState/BasicWatchState/BasicWatchStateProtected.h"
#include "WatchState/WatchState.h"
#include "WatchState/WatchStateProtected.h"

static void
defaultStop(BasicWatchState state)
{
	assert(NULL != state);
	printf("Error: Basic Start event is not supported in this instance.\n");

};

static void
defaultStart(BasicWatchState state)
{
	assert(NULL != state);
	printf("Error: Basic Stop event is not supported in this instance.\n");
};

static void
defaultDestroy(BasicWatchState* state)
{
	assert(NULL != state);
	printf("Error: Basic Destroy event is not supported in this instance.\n");
};

void
DefaultBasicWatchState(BasicWatchState state)
{
	state->start = defaultStart;
	state->stop = defaultStop;
	state->destroy = defaultDestroy;
};

void
basicStart(void* instance)
{
	BasicWatchState state = (BasicWatchState) instance;
	assert(NULL != state);
	return state->start(state);
}

void
basicStop(void* instance)
{
	BasicWatchState state = (BasicWatchState) instance;
	assert(NULL != state);
	return state->stop(state);
}

void
basicDestroy(void* instance)
{
	BasicWatchState basic = (BasicWatchState) instance;
	printf("Destroying BasicWatchState\n");
	free(basic);
}

static struct WatchStateClassMembers
BasicVFT = {
	.className = "Basic",
	.start = basicStart,
	.stop = basicStop,
	.destroy = basicDestroy
};

BasicWatchState
BasicWatchStateNew(WatchState state)
{
	BasicWatchState instance = malloc(sizeof(*instance));
	if (instance) instance->state = state;
	else { fprintf(stderr, "error: base not initialized in BasicWatchStateNew\n"); exit(1); }

	// set stopwatch to the stopped state at creation of new basic watch
	TransitionToStopped(instance);

	state->instance = instance;

	state->vft = &BasicVFT;
	printf("Creating BasicWatchState as state manager\n");

	return instance;
}
