#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "DigitalStopWatch.h"
#include "WatchState/WatchState.h"
#include "WatchState/BasicWatchState/BasicWatchState.h"

struct DigitalStopWatchADT
{
	// interface to a watch state management class
	WatchState state;

	// other components of a digital stop watch:
	// Display interface
	// TimeSource interface
};

DigitalStopWatch
DigitalStopWatchNew(void)
{
	DigitalStopWatch instance = malloc(sizeof *instance);
	assert(NULL != instance);

	WatchState watchState = WatchStateNew();
	assert(NULL != watchState);

	// instantiate a concrete watch state type
	// creating and setting a basic watch state manager
	// could theoretically create another and swap
	BasicWatchStateNew(watchState);
	instance->state = watchState;

	return instance;
};

void
DigitalStopWatchDestroy(DigitalStopWatch* watchPtr)
{
	DigitalStopWatch watch = *watchPtr;
	if (!watch) { fprintf(stderr, "error: double delete of the digital stopwatch\n"); exit(1); }
	if (watch->state) { WatchStateDestroy(&(watch->state)); }
	free(watch);
	*watchPtr = NULL;
}

void
DigitalStopWatchStart(DigitalStopWatch instance)
{
	return WatchStateStart(instance->state);
}

void
DigitalStopWatchStop(DigitalStopWatch instance)
{
	return WatchStateStop(instance->state);
}
