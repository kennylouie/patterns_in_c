#include <stdlib.h>
#include <stdio.h>

#include "customer.h"

int
main(void)
{
	// demo of using the customer api
	CustomerPtr kenny = createCustomer("kenny");

	// use CustomerPtr in other code
	introduceCustomer(kenny);

	// cleanup
	destroyCustomer(kenny);

	return EXIT_SUCCESS;
}
