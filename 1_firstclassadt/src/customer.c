#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "customer.h"

/*
 * complete the incomplete type in private code
 * contains private fields not accessible nor visible to the client
 */
struct Customer
{
	const char* name;
	const char* species;
};

CustomerPtr
createCustomer(
	const char* name
)
{
	CustomerPtr customer = malloc(sizeof *customer);

	assert(customer && "customer exists");
	customer->name = name;
	customer->species = "human";

	return customer;
}

void
introduceCustomer(
	CustomerPtr customer
)
{
	printf("My name is %s and I am a %s.\n", customer->name, customer->species);
}

void
destroyCustomer(
	CustomerPtr customer
)
{
	free(customer);
}
