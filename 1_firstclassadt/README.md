# First Class Abstract Data Type (ADT)

## Background
The aim of this pattern is to encapsulate a private and public interface for classes. Exposing only a public interface for data that is relevant for a client without modification of the internal workings of the class provides a clean interface and segregation.

## Usage

Example trivial program
```
make && ./bin/firstclassadt
```
