#ifndef CUSTOMER_H
#define CUSTOMER_H

/*
 * a pointer to an incomplete type
 * prevents modification of internal implementation
 * exposes public interface
 * a form of OOP encapsulation
 */
typedef struct Customer* CustomerPtr;

CustomerPtr
createCustomer(
	const char* name
);

void
introduceCustomer(
	CustomerPtr customer
);

void
destroyCustomer(
	CustomerPtr customer
);

#endif
